
#include "config.h"

config::config(const char *fileName) :
  SPIFFSIniFile(fileName)
{
  m_fileName = fileName;
}

bool config::read(void)
{
  char buffer[MAX_LINE];
  const char *p;
  char *s;

  if (!open()) {
    Serial.printf("%s: not found\n", m_fileName);
    return false;
  }
  if (getValue("WIFI", "access-point", buffer, sizeof(buffer)) != true) {
    Serial.printf("%s:WIFI:access-point not found (error %d)\n", m_fileName, getError());
    return false;
  }
  p = strtok_r(buffer, ":", &s);
  if (p == NULL) {
    Serial.printf("%s: bad format, missing ':'\n", buffer);
    return false;
  }
  strcpy(m_ssid, p);
  p = strtok_r(NULL, ":", &s);
  if (p == NULL) {
    Serial.printf("%s: missing password\n", buffer);
    return false;
  }
  strcpy(m_password, p);
  return true;
}

void config::print(void)
{
  Serial.printf("ssid: %s\n", m_ssid);
  Serial.printf("password: %s\n", m_password);
}
