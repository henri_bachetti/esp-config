#include <Arduino.h>
#include "relay.h"

relay relay::m_relay[MAX_RELAY];
Adafruit_MCP23017 *relay::m_mcp;
bool relay::m_mcpFound;

bool relay::begin(Adafruit_MCP23017 *mcp, uint8_t address)
{
  m_mcp = mcp;
  mcp->begin(address);
  Wire.beginTransmission(MCP23017_ADDRESS+address);
  byte error = Wire.endTransmission();
  if (error == 0)  {
    Serial.printf("I2C device found at %02x\n", address);
    m_mcpFound = true;
    return true;
  }
  else if (error == 4) {
    Serial.printf("I2C error at %02x\n", address);
  }
  return false;
}

int relay::getCount()
{
  for (int n ; n < MAX_RELAY ; n++) {
    if (!m_relay[n].isPresent()) {
      return n;
    }
  }
  return MAX_RELAY;
}

relay *relay::getRelay(int n)
{
  if (n < MAX_RELAY) {
    return &m_relay[n];
  }
  return 0;
}

bool relay::create(const char *def)
{
  char tmp[MAX_DEF];
  const char *p;
  char *s;

  strcpy(tmp, def);
  p = strtok_r(tmp, "(", &s);
  if (p == NULL) {
    Serial.printf("%s: bad format, missing parenthesis\n", def);
    return false;
  }
  if (!strcmp(p, "GPIO-H")) {
    m_access = IO;
    m_level = HIGH;
  }
  else if (!strcmp(p, "GPIO-L")) {
    m_access = IO;
    m_level = LOW;
  }
  else if (!strcmp(p, "I2C-H")) {
    m_access = I2C;
    m_level = HIGH;
  }
  else if (!strcmp(p, "I2C-L")) {
    m_access = I2C;
    m_level = LOW;
  }
  else {
    Serial.printf("%s: bad value\n", def);
    return false;
  }
  p = strtok_r(NULL, ")", &s);
  if (p == NULL) {
    Serial.printf("%s: bad format, missing parenthesis\n", def);
  }
  if (strchr(p, ',')) {
    strcpy(tmp, p);
    p = strtok_r(tmp, ", ", &s);
    m_onPin = atoi(p);
    p = strtok_r(NULL, ", ", &s);
    m_offPin = atoi(p);
  }
  else {
    m_onPin = atoi(p);
  }
  return true;
}

bool relay::isPresent(void)
{
  return m_onPin != -1 || m_offPin != -1;
}

bool relay::isLatch(void)
{
  if (!isPresent()) return false;
  return m_offPin != -1;
}

void relay::print(void)
{
  if (!isPresent()) {
    Serial.printf("Not present\n");
    return;
  }
  if (isLatch()) {
    Serial.printf("LATCH, %s, %s(%d,%d)\n", m_access == IO ? "GPIO" : "I2C", m_level == HIGH ? "HIGH-LEVEL" : "LOW-LEVEL", m_onPin, m_offPin);
  }
  else {
    Serial.printf("NORMAL, %s, %s(%d)\n", m_access == IO ? "GPIO" : "I2C", m_level == HIGH ? "HIGH-LEVEL" : "LOW-LEVEL", m_onPin);
  }
}

void relay::on(void)
{
  if (!isPresent()) {
    return;
  }
  if (m_access == I2C) {
    if (!m_mcpFound) {
      Serial.printf("MCP not found !!!\n");
      //return;
    }
    if (isLatch()) {
      Serial.printf("MCP%d: %d\n", m_onPin, m_level);
      m_mcp->digitalWrite(m_onPin, m_level);
      delay(50);
      Serial.printf("MCP%d: %d\n", m_onPin, !m_level);
      m_mcp->digitalWrite(m_onPin, !m_level);
    }
    else {
      Serial.printf("MCP%d: %d\n", m_onPin, m_level);
      m_mcp->digitalWrite(m_onPin, m_level);
    }
  }
  else {
    if (isLatch()) {
      Serial.printf("GPIO%d: %d\n", m_onPin, m_level);
      digitalWrite(m_onPin, m_level);
      delay(50);
      Serial.printf("GPIO%d: %d\n", m_onPin, !m_level);
      digitalWrite(m_onPin, !m_level);
    }
    else {
      Serial.printf("GPIO%d: %d\n", m_onPin, m_level);
      digitalWrite(m_onPin, m_level);
    }
  }
}

void relay::off(void)
{
  if (!isPresent()) {
    return;
  }
  if (m_access == I2C) {
    if (!m_mcpFound) {
      Serial.printf("MCP not found !!!\n");
      //return;
    }
    if (isLatch()) {
      Serial.printf("MCP%d: %d\n", m_offPin, m_level);
      m_mcp->digitalWrite(m_onPin, m_level);
      delay(50);
      Serial.printf("MCP%d: %d\n", m_offPin, !m_level);
      m_mcp->digitalWrite(m_onPin, !m_level);
    }
    else {
      Serial.printf("MCP%d: %d\n", m_onPin, !m_level);
      m_mcp->digitalWrite(m_onPin, !m_level);
    }
  }
  else {
    if (isLatch()) {
      Serial.printf("GPIO%d: %d\n", m_offPin, m_level);
      digitalWrite(m_onPin, m_level);
      delay(50);
      Serial.printf("GPIO%d: %d\n", m_offPin, !m_level);
      digitalWrite(m_onPin, !m_level);
    }
    else {
      Serial.printf("GPIO%d: %d\n", m_onPin, !m_level);
      digitalWrite(m_onPin, !m_level);
    }
  }
}
