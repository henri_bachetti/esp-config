
#include <WiFi.h>
#include <esp_wifi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <SPIFFS.h>

#include "config.h"

const char *ssid;
const char *password;

WebServer server(80);

Adafruit_MCP23017 mcp;

const int led = 23;
config config("/config.ini");

void handleRoot() {
  int ledState = digitalRead(led);
  String state = ledState == LOW ? "OFF" : "ON";
  String message = "<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}";
  message += ".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;";
  message += "text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}";
  message += ".button2 {background-color: #555555;}</style></head>";
  message += "<body><h1>ESP32 Web Server</h1>";
  message += "<p>LED 1 - State " + state + "</p>";
  if (ledState == false) {
    message += "<p><a href=\"/on\"><button class=\"button\">ON</button></a></p>";
  } else {
    message += "<p><a href=\"/off\"><button class=\"button button2\">OFF</button></a></p>";
  }

  message += "</body></html>";
  server.send(200, "text/html", message);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  Serial.println("Connected to AP successfully!");
}

void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info) {
  digitalWrite(led, LOW);
  Serial.printf("WiFi connected to %s\n", ssid);
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void WiFiStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  digitalWrite(led, HIGH);
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  Serial.printf("Trying to reconnect to %s\n", ssid);
  WiFi.reconnect();
}

void setup(void)
{
  pinMode(led, OUTPUT);
  digitalWrite(led, HIGH);
  Serial.begin(115200);
  if (!SPIFFS.begin()) {
    Serial.println("SPIFFS.begin() failed");
  }
  if (relay::begin(&mcp) == true) {
    Serial.println("\nMCP23017 found");
  }
  else {
    Serial.println("\nMCP23017 not found !!!");
  }
  config.read();
  config.print();
  WiFi.onEvent(WiFiStationConnected, SYSTEM_EVENT_STA_CONNECTED);
  WiFi.onEvent(WiFiGotIP, SYSTEM_EVENT_STA_GOT_IP);
  WiFi.onEvent(WiFiStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  ssid = config.getSsid();
  password = config.getPassword();
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);

  server.on("/", handleRoot);

  server.on("/inline", []() {
    server.send(200, "text/plain", "This works as well");
  });

  server.on("/on", []() {
    digitalWrite(led, HIGH);
    handleRoot();
  });

  server.on("/off", []() {
    digitalWrite(led, LOW);
    handleRoot();
  });

  server.onNotFound(handleNotFound);

  server.begin();
  //  esp_wifi_set_ps(WIFI_PS_MIN_MODEM);
  Serial.println("HTTP server started");

  Serial.println("RESET all relays");
  for (int n = 0 ; n < relay::getCount() ; n++) {
    relay *relay = relay::getRelay(n);
    relay->off();
  }
}

void loop(void)
{
  server.handleClient();
}
