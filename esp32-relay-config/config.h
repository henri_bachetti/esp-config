
#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <SPIFFS.h>
#include <FS.h>
#include <SPIFFSIniFile.h>

#include "relay.h"

#define MAX_LINE        100

class config : public SPIFFSIniFile
{
  public:
    config(const char *fileName);
    bool read(void);
    void print(void);
    char *getSsid(void) {return m_ssid;}
    char *getPassword(void)  {return m_password;}
  private:
    const char *m_fileName;
    char m_ssid[MAX_LINE/2];
    char m_password[MAX_LINE/2];
};

#endif
