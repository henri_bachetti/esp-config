#ifndef _VALVE_H_
#define _VALVE_H_

#include <Adafruit_MCP23017.h>

#define MAX_DEF         20
#define MAX_RELAY       32

enum accessType {IO, I2C};

class relay
{
  public:
    static bool begin(Adafruit_MCP23017 *mcp, uint8_t address=0);
    static int getCount();
    static relay *getRelay(int n);
    bool create(const char *def);
    relay() {m_onPin = m_offPin = -1; m_level = HIGH;}
    bool isPresent(void);
    bool isLatch(void);
    void print(void);
    void on(void);
    void off(void);
  private:
    static relay m_relay[MAX_RELAY];
    static Adafruit_MCP23017 *m_mcp;
    static bool m_mcpFound;
    accessType m_access;
    int m_onPin;
    int m_offPin;
    int m_level;
};

#endif
