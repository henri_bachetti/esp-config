
#include "config.h"

config::config(const char *fileName) :
  SPIFFSIniFile(fileName)
{
  m_fileName = fileName;
}

bool config::read(void)
{
  char buffer[MAX_LINE];
  const char *p;
  char *s;
  int n;
  relay *relay;

  if (!open()) {
    Serial.printf("%s: not found\n", m_fileName);
    return false;
  }
  if (getValue("WIFI", "access-point", buffer, sizeof(buffer)) != true) {
    Serial.printf("%s:WIFI:access-point not found (error %d)\n", m_fileName, getError());
    return false;
  }
  p = strtok_r(buffer, ":", &s);
  if (p == NULL) {
    Serial.printf("%s: bad format, missing ':'\n", buffer);
    return false;
  }
  strcpy(m_ssid, p);
  p = strtok_r(NULL, ":", &s);
  if (p == NULL) {
    Serial.printf("%s: missing password\n", buffer);
    return false;
  }
  strcpy(m_password, p);
  if (getValue("relays", "main", buffer, sizeof(buffer)) != true) {
    Serial.printf("%s:relays:main not found (error %d)\n", m_fileName, getError());
    return false;
  }
  relay = relay::getRelay(0);
  if (relay->create(buffer) != true) {
    return false;
  }
  if (getValue("relays", "secondary", buffer, sizeof(buffer)) != true) {
    Serial.printf("%s:relays:secondary not found (error %d)\n", m_fileName, getError());
    return false;
  }
  n = 1;
  p = strtok_r(buffer, ", ", &s);
  while (p != NULL) {
    relay = relay::getRelay(n);
    if (relay == 0) {
      Serial.printf("relay %d: not found\n", n);
      return false;
    }
    if (relay->create(p) != true) {
      return false;
    }
    p = strtok_r(NULL, ", ", &s);
    n++;
  }
  return true;
}

void config::print(void)
{
  Serial.printf("ssid: %s\n", m_ssid);
  Serial.printf("password: %s\n", m_password);
  for (int n = 0 ; n < MAX_RELAY ; n++) {
    relay *relay = relay::getRelay(n);
    if (relay->isPresent()) {
      relay->print();
    }
  }
}
